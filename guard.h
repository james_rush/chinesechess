#ifndef GUARD_H
#define GUARD_H

#include "chess.h"
#include <QPushButton>

class guard:public chess
{
    Q_OBJECT
public:
    explicit guard(QWidget *parent = nullptr);
    ~guard();
    void decideAvailbleMoves(QList <QPushButton *>l,chess *chessOnBoard[10][9],QPoint coordinate[10][9],int rx,int ry,int bx,int by);
};
#endif // GUARD_H
