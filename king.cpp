#include "king.h"

king::king(QWidget *parent):chess(parent){

}

king::~king(){

}

void king::decideAvailbleMoves(QList <QPushButton *>l, chess *chessOnBoard[10][9],QPoint coordinate[10][9],int rx,int ry,int bx,int by){
    if(getPlayer() == 0){
        int chessBetween = 0;//check king to king
        for (int i = ry ;i >= by ;--i) {//check chess between
            if(chessOnBoard[i][rx] != nullptr){
                chessBetween++;
            }
        }
        if(!(rx == getPos().second + 1 && chessBetween == 1) && getPos().second + 1 <= 5){
            generateAvailbleMove(getPos().first,getPos().second + 1,l,2,chessOnBoard,getPlayer(),coordinate);//move right
        }
        if(getPos().first + 1 <= 2){
            generateAvailbleMove(getPos().first + 1,getPos().second,l,1,chessOnBoard,getPlayer(),coordinate);//move forward
        }
        if(!(rx == getPos().second - 1 && chessBetween == 1) && getPos().second - 1 >= 3){
            generateAvailbleMove(getPos().first,getPos().second - 1,l,3,chessOnBoard,getPlayer(),coordinate);//move left
        }
        generateAvailbleMove(getPos().first - 1,getPos().second,l,4,chessOnBoard,getPlayer(),coordinate);//move backward
    }
    if(getPlayer() == 1){
        int chessBetween = 0;
        for (int i = by;i <= ry;++i) {//check king to king
            if(chessOnBoard[i][bx] != nullptr){
                chessBetween++;
            }
        }
        if(!(bx == getPos().second + 1 && chessBetween == 1) && getPos().second + 1 <= 5){
            generateAvailbleMove(getPos().first,getPos().second + 1,l,2,chessOnBoard,getPlayer(),coordinate);//move right
        }
        if(getPos().first - 1 >= 7){
            generateAvailbleMove(getPos().first - 1,getPos().second,l,1,chessOnBoard,getPlayer(),coordinate);//move forward
        }
        if(!(bx == getPos().second - 1 && chessBetween == 1) && getPos().second - 1 >= 2){
            generateAvailbleMove(getPos().first,getPos().second - 1,l,3,chessOnBoard,getPlayer(),coordinate);//move left
        }
        generateAvailbleMove(getPos().first + 1,getPos().second,l,4,chessOnBoard,getPlayer(),coordinate);//move backward
    }
}


