#ifndef CHESS_H
#define CHESS_H

#include <QPoint>
#include <utility>
#include <QPushButton>
#include <QMainWindow>

class chess:public QPushButton
{
    Q_OBJECT
public:
    explicit chess(QWidget *parent = nullptr);
    ~chess();
    void setPos(int y,int x);
    void setLife(bool alive);
    void setPlayer(bool color);
    void generateAvailbleMove(int y,int x,QList <QPushButton *>l,int num,chess *chessOnBoard[10][9],bool idenityOfChessToMove,QPoint coordinate[10][9]);
    std::pair<int,int> getPos();
    bool getLife();
    bool getPlayer();


private:
    bool life;
    bool player; // black = 0 red = 1
    std::pair<int,int> pos;//(y,x)
};

#endif // CHESS_H
