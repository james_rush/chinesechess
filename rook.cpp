#include "rook.h"
#include <QtDebug>

rook::rook(QWidget *parent):chess(parent){

}

rook::~rook(){

}

void rook::decideAvailbleMoves(QList <QPushButton *>l, chess *chessOnBoard[10][9],QPoint coordinate[10][9],int rx,int ry,int bx,int by){
    int movesForPositiveY,movesForNegativeY,movesForPositiveX,movesForNegativeX,totalMoves = 0;
    bool kingToKing = false;
    int chessBetween = 0;
    for (int i = by;i <= ry;++i) {//check king to king
        if(chessOnBoard[i][rx] != nullptr){
            chessBetween++;
        }
    }
    if(rx == bx && rx == getPos().second && chessBetween == 3){
        kingToKing = true;
    }
    //+y
    for(movesForPositiveY = 1;movesForPositiveY < 10 ;++movesForPositiveY){//check how many moves availble in +y
        if(chessOnBoard[getPos().first + movesForPositiveY][getPos().second] != nullptr || getPos().first + movesForPositiveY == 9){
            break;
        }
    }

    for(int i = 1;i <=movesForPositiveY;++i){//set red block on +y
        generateAvailbleMove(getPos().first + i,getPos().second,l,i,chessOnBoard,getPlayer(),coordinate);
    }

    totalMoves += movesForPositiveY;//refresh total moves
    //+x
    for(movesForPositiveX = 1;movesForPositiveX < 9;++movesForPositiveX){//check how many moves availble in +x
        if(chessOnBoard[getPos().first ][getPos().second + movesForPositiveX] != nullptr || getPos().second + movesForPositiveX == 8){
            break;
        }
    }

    for(int i = 1;i <= movesForPositiveX;++i){//set red block on +x
        if(!kingToKing){
            generateAvailbleMove(getPos().first,getPos().second + i,l, totalMoves + i,chessOnBoard,getPlayer(),coordinate);
        }
    }

    totalMoves += movesForPositiveX;//refresh total moves

    //-y
    for(movesForNegativeY = 1;movesForNegativeY < 10;++movesForNegativeY){//check how many moves availble in -y
        if(chessOnBoard[getPos().first - movesForNegativeY ][getPos().second] != nullptr || getPos().first - movesForNegativeY == 0){
            break;
        }
    }

    for(int i = 1;i <= movesForNegativeY;++i){//set red block on -y
        generateAvailbleMove(getPos().first - i,getPos().second,l, totalMoves + i,chessOnBoard,getPlayer(),coordinate);
    }

    totalMoves += movesForNegativeY;//refresh total moves

    //-x
    for(movesForNegativeX = 1;movesForNegativeX < 9 ;++movesForNegativeX){//check how many moves availble in -x
        if(chessOnBoard[getPos().first][getPos().second - movesForNegativeX] != nullptr || getPos().second - movesForNegativeX == 0){//check obsticle on road and edges
            break;
        }
    }
    for(int i = 1;i <= movesForNegativeX;++i){//set red block on -x
        if(!kingToKing){
            generateAvailbleMove(getPos().first,getPos().second - i,l, totalMoves + i,chessOnBoard,getPlayer(),coordinate);
        }
    }

}

