#include "horse.h"

horse::horse(QWidget *parent):chess(parent){

}

horse::~horse(){

}

void horse::decideAvailbleMoves(QList <QPushButton *>l, chess *chessOnBoard[10][9],QPoint coordinate[10][9],int rx,int ry,int bx,int by){
    bool kingToKing = false;
    int chessBetween = 0;
    for (int i = by;i <= ry;++i) {//check king to king
        if(chessOnBoard[i][rx] != nullptr){
            chessBetween++;
        }
    }
    if(rx == bx && rx == getPos().second && chessBetween == 3){
        kingToKing = true;
    }
    if(!kingToKing){
        //+y
        if(chessOnBoard[getPos().first + 1][getPos().second] == nullptr){//special rule
            generateAvailbleMove(getPos().first + 2,getPos().second + 1,l,1,chessOnBoard,getPlayer(),coordinate);
            generateAvailbleMove(getPos().first + 2,getPos().second - 1,l,2,chessOnBoard,getPlayer(),coordinate);
        }

        //-y
        if(chessOnBoard[getPos().first - 1][getPos().second] == nullptr){//special rule
            generateAvailbleMove(getPos().first - 2,getPos().second + 1,l,3,chessOnBoard,getPlayer(),coordinate);
            generateAvailbleMove(getPos().first - 2,getPos().second - 1,l,4,chessOnBoard,getPlayer(),coordinate);
        }

        //+x
        if(chessOnBoard[getPos().first][getPos().second + 1] == nullptr){//special rule
            generateAvailbleMove(getPos().first + 1,getPos().second + 2,l,5,chessOnBoard,getPlayer(),coordinate);
            generateAvailbleMove(getPos().first - 1,getPos().second + 2,l,6,chessOnBoard,getPlayer(),coordinate);
        }
        //-x
        if(chessOnBoard[getPos().first][getPos().second - 1] == nullptr){//special rule
            generateAvailbleMove(getPos().first + 1,getPos().second - 2,l,7,chessOnBoard,getPlayer(),coordinate);
            generateAvailbleMove(getPos().first - 1,getPos().second - 2,l,8,chessOnBoard,getPlayer(),coordinate);
        }
    }
}
