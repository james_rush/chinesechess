#ifndef HORSE_H
#define HORSE_H

#include "chess.h"
#include <QPushButton>

class horse:public chess
{
    Q_OBJECT
public:
    explicit horse(QWidget *parent = nullptr);
    ~horse();
    void decideAvailbleMoves(QList <QPushButton *>l,chess *chessOnBoard[10][9],QPoint coordinate[10][9],int rx,int ry,int bx,int by);
};


#endif // HORSE_H
