#ifndef ELEPHANT_H
#define ELEPHANT_H

#include "chess.h"
#include <QPushButton>

class elephant:public chess
{
    Q_OBJECT
public:
    explicit elephant(QWidget *parent = nullptr);
    ~elephant();
    void decideAvailbleMoves(QList <QPushButton *>l,chess *chessOnBoard[10][9],QPoint coordinate[10][9],int rx,int ry,int bx,int by);
};

#endif // ELEPHANT_H
