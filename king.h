#ifndef KING_H
#define KING_H

#include "chess.h"
#include <QPushButton>

class king:public chess
{
    Q_OBJECT
public:
    explicit king(QWidget *parent = nullptr);
    ~king();
    bool checkKingToKing();
    void decideAvailbleMoves(QList <QPushButton *>l,chess *chessOnBoard[10][9],QPoint coordinate[10][9],int rx,int ry,int bx,int by);
};

#endif // KING_H
