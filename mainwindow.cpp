#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "chess.h"
#include <QPushButton>
#include <QFile>


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),chessOnBoard{{nullptr}},whoseTurn(true)
{
    ui->setupUi(this);
    //set coordinate
    for(int i = 0;i < 10;++i){
        for(int j = 0;j < 9;++j){
            coordinate[i][j].setY(10 + 50 *i);
            coordinate[i][j].setX(10 + 50 *j);
        }
    }

    manageMoveBlocks();
    hideAllMoveBlocks();
    initializeChesses();
    manageChess();

    for(int i = 0;i < availbleMoves.size();++i){//connect all redblock with slot
        connect(availbleMoves.at(i), SIGNAL(clicked()), this, SLOT(availblemove_clicked()));
    }
    changeTurn();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::initializeChesses(){
    //hide middle lables and restart
    ui->blue_win->setVisible(false);
    ui->red_win->setVisible(false);
    ui->restart->setVisible(false);
    //initialize black chesses
    //blackpawn
    ui->blackpawn_1->move(coordinate[3][0].x(),coordinate[3][0].y());
    chessOnBoard[3][0] = ui->blackpawn_1;
    ui->blackpawn_1->setPlayer(0);
    ui->blackpawn_1->setPos(3,0);
    connect(ui->blackpawn_1, SIGNAL(clicked()), this, SLOT(pawn_clicked()));

    ui->blackpawn_2->move(coordinate[3][2].x(),coordinate[3][2].y());
    chessOnBoard[3][2] = ui->blackpawn_2;
    ui->blackpawn_2->setPlayer(0);
    ui->blackpawn_2->setPos(3,2);
    connect(ui->blackpawn_2, SIGNAL(clicked()), this, SLOT(pawn_clicked()));

    ui->blackpawn_3->move(coordinate[3][4].x(),coordinate[3][4].y());
    chessOnBoard[3][4] = ui->blackpawn_3;
    ui->blackpawn_3->setPlayer(0);
    ui->blackpawn_3->setPos(3,4);
     connect(ui->blackpawn_3, SIGNAL(clicked()), this, SLOT(pawn_clicked()));

    ui->blackpawn_4->move(coordinate[3][6].x(),coordinate[3][6].y());
    chessOnBoard[3][6] = ui->blackpawn_4;
    ui->blackpawn_4->setPlayer(0);
    ui->blackpawn_4->setPos(3,6);
    connect(ui->blackpawn_4, SIGNAL(clicked()), this, SLOT(pawn_clicked()));

    ui->blackpawn_5->move(coordinate[3][8].x(),coordinate[3][8].y());
    chessOnBoard[3][8] = ui->blackpawn_5;
    ui->blackpawn_5->setPlayer(0);
    ui->blackpawn_5->setPos(3,8);
    connect(ui->blackpawn_5, SIGNAL(clicked()), this, SLOT(pawn_clicked()));

    //black rook
    ui->blackrook_1->move(coordinate[0][0].x(),coordinate[0][0].y());
    chessOnBoard[0][0] = ui->blackrook_1;
    ui->blackrook_1->setPlayer(0);
    ui->blackrook_1->setPos(0,0);
    connect(ui->blackrook_1, SIGNAL(clicked()), this, SLOT(rook_clicked()));

    ui->blackrook_2->move(coordinate[0][8].x(),coordinate[0][8].y());
    chessOnBoard[0][8] = ui->blackrook_2;
    ui->blackrook_2->setPlayer(0);
    ui->blackrook_2->setPos(0,8);
    connect(ui->blackrook_2, SIGNAL(clicked()), this, SLOT(rook_clicked()));

    //black horse
    ui->blackhorse_1->move(coordinate[0][1].x(),coordinate[0][1].y());
    chessOnBoard[0][1] = ui->blackhorse_1;
    ui->blackhorse_1->setPlayer(0);
    ui->blackhorse_1->setPos(0,1);
    connect(ui->blackhorse_1, SIGNAL(clicked()), this, SLOT(horse_clicked()));

    ui->blackhorse_2->move(coordinate[0][7].x(),coordinate[0][7].y());
    chessOnBoard[0][7] = ui->blackhorse_2;
    ui->blackhorse_2->setPlayer(0);
    ui->blackhorse_2->setPos(0,7);
    connect(ui->blackhorse_2, SIGNAL(clicked()), this, SLOT(horse_clicked()));

    //black elephant
    ui->blackelephant_1->move(coordinate[0][2].x(),coordinate[0][2].y());
    chessOnBoard[0][2] = ui->blackelephant_1;
    ui->blackelephant_1->setPlayer(0);
    ui->blackelephant_1->setPos(0,2);
    connect(ui->blackelephant_1, SIGNAL(clicked()), this, SLOT(elephant_clicked()));

    ui->blackelephant_2->move(coordinate[0][6].x(),coordinate[0][6].y());
    chessOnBoard[0][6] = ui->blackelephant_2;
    ui->blackelephant_2->setPlayer(0);
    ui->blackelephant_2->setPos(0,6);
    connect(ui->blackelephant_2, SIGNAL(clicked()), this, SLOT(elephant_clicked()));

    //black guard
    ui->blackguard_1->move(coordinate[0][3].x(),coordinate[0][3].y());
    chessOnBoard[0][3] = ui->blackguard_1;
    ui->blackguard_1->setPlayer(0);
    ui->blackguard_1->setPos(0,3);
    connect(ui->blackguard_1, SIGNAL(clicked()), this, SLOT(guard_clicked()));

    ui->blackguard_2->move(coordinate[0][5].x(),coordinate[0][5].y());
    chessOnBoard[0][5] = ui->blackguard_2;
    ui->blackguard_2->setPlayer(0);
    ui->blackguard_2->setPos(0,5);
    connect(ui->blackguard_2, SIGNAL(clicked()), this, SLOT(guard_clicked()));

    //black cannon
    ui->blackcannon_1->move(coordinate[2][1].x(),coordinate[2][1].y());
    chessOnBoard[2][1] = ui->blackcannon_1;
    ui->blackcannon_1->setPlayer(0);
    ui->blackcannon_1->setPos(2,1);
    connect(ui->blackcannon_1, SIGNAL(clicked()), this, SLOT(cannon_clicked()));

    ui->blackcannon_2->move(coordinate[2][7].x(),coordinate[2][7].y());
    chessOnBoard[2][7] = ui->blackcannon_2;
    ui->blackcannon_2->setPlayer(0);
    ui->blackcannon_2->setPos(2,7);
    connect(ui->blackcannon_2, SIGNAL(clicked()), this, SLOT(cannon_clicked()));

    //black king
    ui->blackking->move(coordinate[0][4].x(),coordinate[0][4].y());
    chessOnBoard[0][4] = ui->blackking;
    ui->blackking->setPlayer(0);
    ui->blackking->setPos(0,4);
    connect(ui->blackking, SIGNAL(clicked()), this, SLOT(king_clicked()));

    //initialize red chesses
    //redpawn
    ui->redpawn_1->move(coordinate[6][0].x(),coordinate[6][0].y());
    chessOnBoard[6][0] = ui->redpawn_1;
    ui->redpawn_1->setPlayer(1);
    ui->redpawn_1->setPos(6,0);
    connect(ui->redpawn_1, SIGNAL(clicked()), this, SLOT(pawn_clicked()));

    ui->redpawn_2->move(coordinate[6][2].x(),coordinate[6][2].y());
    chessOnBoard[6][2] = ui->redpawn_2;
    ui->redpawn_2->setPlayer(1);
    ui->redpawn_2->setPos(6,2);
    connect(ui->redpawn_2, SIGNAL(clicked()), this, SLOT(pawn_clicked()));

    ui->redpawn_3->move(coordinate[6][4].x(),coordinate[6][4].y());
    chessOnBoard[6][4] = ui->redpawn_3;
    ui->redpawn_3->setPlayer(1);
    ui->redpawn_3->setPos(6,4);
    connect(ui->redpawn_3, SIGNAL(clicked()), this, SLOT(pawn_clicked()));

    ui->redpawn_4->move(coordinate[6][6].x(),coordinate[6][6].y());
    chessOnBoard[6][6] = ui->redpawn_4;
    ui->redpawn_4->setPlayer(1);
    ui->redpawn_4->setPos(6,6);
    connect(ui->redpawn_4, SIGNAL(clicked()), this, SLOT(pawn_clicked()));

    ui->redpawn_5->move(coordinate[6][8].x(),coordinate[6][8].y());
    chessOnBoard[6][8] = ui->redpawn_5;
    ui->redpawn_5->setPlayer(1);
    ui->redpawn_5->setPos(6,8);
    connect(ui->redpawn_5, SIGNAL(clicked()), this, SLOT(pawn_clicked()));

    //red rook
    ui->redrook_1->move(coordinate[9][0].x(),coordinate[9][0].y());
    chessOnBoard[9][0] = ui->redrook_1;
    ui->redrook_1->setPlayer(1);
    ui->redrook_1->setPos(9,0);
    connect(ui->redrook_1, SIGNAL(clicked()), this, SLOT(rook_clicked()));

    ui->redrook_2->move(coordinate[9][8].x(),coordinate[9][8].y());
    chessOnBoard[9][0] = ui->redrook_2;
    ui->redrook_2->setPlayer(1);
    ui->redrook_2->setPos(9,8);
    connect(ui->redrook_2, SIGNAL(clicked()), this, SLOT(rook_clicked()));

    //red horse
    ui->redhorse_1->move(coordinate[9][1].x(),coordinate[9][1].y());
    chessOnBoard[9][1] = ui->redhorse_1;
    ui->redhorse_1->setPlayer(1);
    ui->redhorse_1->setPos(9,1);
    connect(ui->redhorse_1, SIGNAL(clicked()), this, SLOT(horse_clicked()));

    ui->redhorse_2->move(coordinate[9][7].x(),coordinate[9][7].y());
    chessOnBoard[9][7] = ui->redhorse_2;
    ui->redhorse_2->setPlayer(1);
    ui->redhorse_2->setPos(9,7);
    connect(ui->redhorse_2, SIGNAL(clicked()), this, SLOT(horse_clicked()));

    //red elephant
    ui->redelephant_1->move(coordinate[9][2].x(),coordinate[9][2].y());
    chessOnBoard[9][2] = ui->redelephant_1;
    ui->redelephant_1->setPlayer(1);
    ui->redelephant_1->setPos(9,2);
    connect(ui->redelephant_1, SIGNAL(clicked()), this, SLOT(elephant_clicked()));

    ui->redelephant_2->move(coordinate[9][6].x(),coordinate[9][6].y());
    chessOnBoard[9][6] = ui->redelephant_2;
    ui->redelephant_2->setPlayer(1);
    ui->redelephant_2->setPos(9,6);
    connect(ui->redelephant_2, SIGNAL(clicked()), this, SLOT(elephant_clicked()));

    //red guard
    ui->redguard_1->move(coordinate[9][3].x(),coordinate[9][3].y());
    chessOnBoard[9][3] = ui->redguard_1;
    ui->redguard_1->setPlayer(1);
    ui->redguard_1->setPos(9,3);
    connect(ui->redguard_1, SIGNAL(clicked()), this, SLOT(guard_clicked()));

    ui->redguard_2->move(coordinate[9][5].x(),coordinate[9][5].y());
    chessOnBoard[9][5] = ui->redguard_2;
    ui->redguard_2->setPlayer(1);
    ui->redguard_2->setPos(9,5);
    connect(ui->redguard_2, SIGNAL(clicked()), this, SLOT(guard_clicked()));

    //red cannon
    ui->redcannon_1->move(coordinate[7][1].x(),coordinate[7][1].y());
    chessOnBoard[7][1] = ui->redcannon_1;
    ui->redcannon_1->setPlayer(1);
    ui->redcannon_1->setPos(7,1);
    connect(ui->redcannon_1, SIGNAL(clicked()), this, SLOT(cannon_clicked()));

    ui->redcannon_2->move(coordinate[7][7].x(),coordinate[7][7].y());
    chessOnBoard[7][7] = ui->redcannon_2;
    ui->redcannon_2->setPlayer(1);
    ui->redcannon_2->setPos(7,7);
    connect(ui->redcannon_2, SIGNAL(clicked()), this, SLOT(cannon_clicked()));

    //red king
    ui->redking->move(coordinate[9][4].x(),coordinate[9][4].y());
    chessOnBoard[9][4] = ui->redking;
    ui->redking->setPlayer(1);
    ui->redking->setPos(9,4);
    connect(ui->redking, SIGNAL(clicked()), this, SLOT(king_clicked()));

}

void MainWindow::manageMoveBlocks(){
    availbleMoves.push_back(ui->availblemove_1);
    availbleMoves.push_back(ui->availblemove_2);
    availbleMoves.push_back(ui->availblemove_3);
    availbleMoves.push_back(ui->availblemove_4);
    availbleMoves.push_back(ui->availblemove_5);
    availbleMoves.push_back(ui->availblemove_6);
    availbleMoves.push_back(ui->availblemove_7);
    availbleMoves.push_back(ui->availblemove_8);
    availbleMoves.push_back(ui->availblemove_9);
    availbleMoves.push_back(ui->availblemove_10);
    availbleMoves.push_back(ui->availblemove_11);
    availbleMoves.push_back(ui->availblemove_12);
    availbleMoves.push_back(ui->availblemove_13);
    availbleMoves.push_back(ui->availblemove_14);
    availbleMoves.push_back(ui->availblemove_15);
    availbleMoves.push_back(ui->availblemove_16);
    availbleMoves.push_back(ui->availblemove_17);
    availbleMoves.push_back(ui->availblemove_18);
    availbleMoves.push_back(ui->availblemove_19);
    availbleMoves.push_back(ui->availblemove_20);
    availbleMoves.push_back(ui->availblemove_21);
    availbleMoves.push_back(ui->availblemove_22);
    availbleMoves.push_back(ui->availblemove_23);
    availbleMoves.push_back(ui->availblemove_24);
    availbleMoves.push_back(ui->availblemove_25);
}

void MainWindow::manageChess(){//pawn rook horse cannon elephant guard king
    //blackchess 16
    blackChess.push_back(ui->blackpawn_1);
    blackChess.push_back(ui->blackpawn_2);
    blackChess.push_back(ui->blackpawn_3);
    blackChess.push_back(ui->blackpawn_4);
    blackChess.push_back(ui->blackpawn_5);
    blackChess.push_back(ui->blackrook_1);
    blackChess.push_back(ui->blackrook_2);
    blackChess.push_back(ui->blackhorse_1);
    blackChess.push_back(ui->blackhorse_2);
    blackChess.push_back(ui->blackcannon_1);
    blackChess.push_back(ui->blackcannon_2);
    blackChess.push_back(ui->blackelephant_1);
    blackChess.push_back(ui->blackelephant_2);
    blackChess.push_back(ui->blackguard_1);
    blackChess.push_back(ui->blackguard_2);
    blackChess.push_back(ui->blackking);

    //redchess 16
    redChess.push_back(ui->redpawn_1);
    redChess.push_back(ui->redpawn_2);
    redChess.push_back(ui->redpawn_3);
    redChess.push_back(ui->redpawn_4);
    redChess.push_back(ui->redpawn_5);
    redChess.push_back(ui->redrook_1);
    redChess.push_back(ui->redrook_2);
    redChess.push_back(ui->redhorse_1);
    redChess.push_back(ui->redhorse_2);
    redChess.push_back(ui->redcannon_1);
    redChess.push_back(ui->redcannon_2);
    redChess.push_back(ui->redelephant_1);
    redChess.push_back(ui->redelephant_2);
    redChess.push_back(ui->redguard_1);
    redChess.push_back(ui->redguard_2);
    redChess.push_back(ui->redking);
}

void MainWindow::hideAllMoveBlocks(){
    for(int i = 0;i < availbleMoves.size();++i){
        availbleMoves[i]->setVisible(false);
    }
}

void MainWindow::pawn_clicked()
{
    pawn* sender = qobject_cast<pawn*>(QObject::sender());
    hideAllMoveBlocks();
    sender->decideAvailbleMoves(availbleMoves,chessOnBoard,coordinate,ui->redking->getPos().second,ui->redking->getPos().first,ui->blackking->getPos().second,ui->blackking->getPos().first);
    toMove = qobject_cast<chess*>(sender);//pass sender out
}

void MainWindow::rook_clicked()
{
    rook* sender = qobject_cast<rook*>(QObject::sender());
    hideAllMoveBlocks();
    sender->decideAvailbleMoves(availbleMoves,chessOnBoard,coordinate,ui->redking->getPos().second,ui->redking->getPos().first,ui->blackking->getPos().second,ui->blackking->getPos().first);
    toMove = qobject_cast<chess*>(sender);
}

void MainWindow::horse_clicked()
{
    horse* sender = qobject_cast<horse*>(QObject::sender());
    hideAllMoveBlocks();
    sender->decideAvailbleMoves(availbleMoves,chessOnBoard,coordinate,ui->redking->getPos().second,ui->redking->getPos().first,ui->blackking->getPos().second,ui->blackking->getPos().first);
    toMove = qobject_cast<chess*>(sender);
}

void MainWindow::elephant_clicked()
{
    elephant* sender = qobject_cast<elephant*>(QObject::sender());
    hideAllMoveBlocks();
    sender->decideAvailbleMoves(availbleMoves,chessOnBoard,coordinate,ui->redking->getPos().second,ui->redking->getPos().first,ui->blackking->getPos().second,ui->blackking->getPos().first);
    toMove = qobject_cast<chess*>(sender);
}


void MainWindow::guard_clicked()
{
    guard* sender = qobject_cast<guard*>(QObject::sender());
    hideAllMoveBlocks();
    sender->decideAvailbleMoves(availbleMoves,chessOnBoard,coordinate,ui->redking->getPos().second,ui->redking->getPos().first,ui->blackking->getPos().second,ui->blackking->getPos().first);
    toMove = qobject_cast<chess*>(sender);
}

void MainWindow::cannon_clicked()
{
    cannon* sender = qobject_cast<cannon*>(QObject::sender());
    hideAllMoveBlocks();
    sender->decideAvailbleMoves(availbleMoves,chessOnBoard,coordinate,ui->redking->getPos().second,ui->redking->getPos().first,ui->blackking->getPos().second,ui->blackking->getPos().first);
    toMove = qobject_cast<chess*>(sender);
}

void MainWindow::king_clicked()
{
    king* sender = qobject_cast<king*>(QObject::sender());
    hideAllMoveBlocks();
    sender->decideAvailbleMoves(availbleMoves,chessOnBoard,coordinate,ui->redking->getPos().second,ui->redking->getPos().first,ui->blackking->getPos().second,ui->blackking->getPos().first);
    toMove = qobject_cast<chess*>(sender);
}


void MainWindow::availblemove_clicked()
{
    QPushButton* sender = qobject_cast<QPushButton*>(QObject::sender());
    int x = (sender->x() - 10) / 50,y = (sender->y() - 10) / 50;
    hideAllMoveBlocks();//hide red block
    chessOnBoard[toMove->getPos().first][toMove->getPos().second] = nullptr;//clear orginal record in chessOnBoard
    toMove->move(sender->x(),sender->y());//move chess
    toMove->setPos(y,x);//refresh position on  moved chess
    checkEat(y,x);//check if eat any enemy
    chessOnBoard[y][x] = toMove;//refresh record in chessOnBoard
    if(!checkWin()){
        changeTurn();
    }
}

void MainWindow::checkEat(int y, int x){
    if(chessOnBoard[y][x] != nullptr){
       chessOnBoard[y][x]->hide();//hide enemy chess
       chessOnBoard[y][x]->setLife(false);// kill enemy chess
       chessOnBoard[y][x] = nullptr;// clear record in chessOnboard
    }
}

void MainWindow::changeTurn(){
    if(whoseTurn){
        //red turn
        for(int i = 0;i <redChess.size();++i){
            redChess.at(i)->setEnabled(true);
        }
        //black freeze
        for(int i = 0;i <blackChess.size();++i){
            blackChess.at(i)->setEnabled(false);
        }
        //change turns
        whoseTurn = false;
    }
    else{
        //black turn
        for(int i = 0;i <blackChess.size();++i){
            blackChess.at(i)->setEnabled(true);
        }
        //red freeze
        for(int i = 0;i <redChess.size();++i){
            redChess.at(i)->setEnabled(false);
        }
        //change turns
        whoseTurn = true;
    }
}


bool MainWindow::checkWin(){
    if(!(ui->blackking->getLife())){//black king die
        ui->red_win->setVisible(true);
        ui->restart->setVisible(true);
        //black freeze
        for(int i = 0;i <blackChess.size();++i){
            blackChess.at(i)->setEnabled(false);
        }
        //red freeze
        for(int i = 0;i <redChess.size();++i){
            redChess.at(i)->setEnabled(false);
        }
        return true;
    }
    else if(!(ui->redking->getLife())){//red king die
        ui->restart->setVisible(true);
        ui->blue_win->setVisible(true);
        //black freeze
        for(int i = 0;i <blackChess.size();++i){
            blackChess.at(i)->setEnabled(false);
        }
        //red freeze
        for(int i = 0;i <redChess.size();++i){
            redChess.at(i)->setEnabled(false);
        }
        return true;
    }
    else {
        return false;
    }
}

void MainWindow::on_restart_clicked()
{   //revive all chess
    for(int i = 0;i <blackChess.size();++i){
        blackChess.at(i)->setLife(true);
        blackChess.at(i)->setVisible(true);
    }
    for(int i = 0;i <redChess.size();++i){
        redChess.at(i)->setLife(true);
        redChess.at(i)->setVisible(true);
    }
    //clear chess On Board
    for (int i = 0;i < 10;++i) {
        for (int j = 0;j < 9;++j) {
            chessOnBoard[i][j] = nullptr;
        }
    }
    initializeChesses();
    whoseTurn = true;
    changeTurn();
}

void MainWindow::on_pushButton_clicked()
{
    QString path = qApp->applicationDirPath() + "/save.txt";
    qDebug()<< "Save file at " << path <<endl;
    QFile file(path);
    QTextStream out(&file);

    //avoid illegal moves
    hideAllMoveBlocks();
    //hide middle lables and restart
    ui->blue_win->setVisible(false);
    ui->red_win->setVisible(false);
    ui->restart->setVisible(false);

    if (!file.open(QIODevice::WriteOnly | QIODevice::Text)){
        qDebug()<<"file open failed"<<endl;
    }
    //print chess data into save.txt
    out <<"black chess" << endl;
    for (int i = 0;i < blackChess.size();++i) {
        out << blackChess[i]->objectName() << endl;
        out << blackChess[i]->getPos().first << endl;//(Y,X)
        out << blackChess[i]->getPos().second << endl;
        out << blackChess[i]->getLife()<<endl;
    }
    out << "red chess"<<endl;
    for (int i = 0;i < redChess.size();++i) {
        out << redChess[i]->objectName() << endl;
        out << redChess[i]->getPos().first << endl;//(Y,X)
        out << redChess[i]->getPos().second << endl;
        out << redChess[i]->getLife()<<endl;
    }
    out << "whoseTurn" << endl;
    out << whoseTurn;
    file.close();
}

void MainWindow::on_pushButton_2_clicked()
{
    QString path = qApp->applicationDirPath() + "/save.txt";
    QFile file(path);
    QTextStream in(&file);
    int x = 0,y = 0,life = 2,turn = 2;
    QString trash;

    //avoid illegal moves
    hideAllMoveBlocks();
    //hide middle lables and restart
    ui->blue_win->setVisible(false);
    ui->red_win->setVisible(false);
    ui->restart->setVisible(false);
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text)){
        qDebug()<<"file open failed"<<endl;
    }
    for (int i = 0;i < 10;++i) {//clear chess on board
        for (int j = 0;j < 9;++j) {
            chessOnBoard[i][j] = nullptr;
        }
    }
    //read data
    trash = in.readLine();  //read "black chess"
    for (int i = 0;i < blackChess.size();++i) {//black chess
        trash = in.readLine();
        y = in.readLine().toInt();
        x = in.readLine().toInt();
        life = in.readLine().toInt();
        if(life == 1){ // check life
            blackChess[i]->setLife(true);
            blackChess[i]->setVisible(true);
            blackChess[i]->setPos(y,x);
            blackChess[i]->move(coordinate[y][x].x(),coordinate[y][x].y());
            chessOnBoard[y][x] = blackChess[i];
        }
        else {
            blackChess[i]->setLife(false);
            blackChess[i]->setVisible(false);
        }
    }
    trash = in.readLine();  //read "red chess"
    for (int i = 0;i < redChess.size();++i) {//red chess
        trash = in.readLine();
        y = in.readLine().toInt();
        x = in.readLine().toInt();
        life = in.readLine().toInt();
        if(life == 1){// check life
            redChess[i]->setLife(true);
            redChess[i]->setVisible(true);
            redChess[i]->setPos(y,x);
            redChess[i]->move(coordinate[y][x].x(),coordinate[y][x].y());
            chessOnBoard[y][x] = redChess[i];
        }
        else {
            redChess[i]->setLife(false);
            redChess[i]->setVisible(false);
        }
    }
    trash = in.readLine(); //read "whoseTurn"
    turn = in.readLine().toInt();
    if(turn == 0){
        whoseTurn = true;
    }
    else {
        whoseTurn = false;
    }
    changeTurn();//keep turn correct
    file.close();
}
