#ifndef ROOK_H
#define ROOK_H

#include "chess.h"
#include <QPushButton>

class rook : public chess
{
    Q_OBJECT
public:
    explicit rook(QWidget *parent = nullptr);
    ~rook();
    void decideAvailbleMoves(QList <QPushButton *>l,chess *chessOnBoard[10][9],QPoint coordinate[10][9],int rx,int ry,int bx,int by);
};

#endif // ROOK_H
