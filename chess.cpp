#include "chess.h"
#include <QMainWindow>
#include <QDebug>

chess::chess(QWidget* parent):QPushButton (parent)
{
    setLife(true);
}

void chess::setPos(int y,int x){
    pos = std::make_pair(y,x);
}
void chess::setLife(bool alive){
    life = alive;
}

void chess::setPlayer(bool color){
    player = color;
}

std::pair<int,int> chess::getPos(){
    return pos;
}
bool chess::getLife(){
    return life;
}

bool chess::getPlayer(){
    return player;
}

void chess::generateAvailbleMove(int y, int x,QList <QPushButton *>l,int num,chess *chessOnBoard[10][9],bool idenityOfChessToMove,QPoint coordinate[10][9]){
    int index = num - 1;//index of red box
    if(y >= 0 && y < 10 && x >= 0 && x < 9){
        if(chessOnBoard[y][x] == nullptr ||chessOnBoard[y][x]->getPlayer() != idenityOfChessToMove){//checkfriendly fire
            l.at(index)->move(coordinate[y][x].x(),coordinate[y][x].y());//set red block
            l.at(index)->setVisible(true);
        }
    }
}

chess::~chess(){

}
