#ifndef PAWN_H
#define PAWN_H

#include "chess.h"
#include <QPushButton>

class pawn:public chess
{
    Q_OBJECT
public:
    explicit pawn(QWidget *parent = nullptr);
    ~pawn();
    void decideAvailbleMoves(QList <QPushButton *>l,chess *chessOnBoard[10][9],QPoint coordinate[10][9],int rx,int ry,int bx,int by);
};

#endif // PAWN_H
