#include "pawn.h"
#include <QDebug>

pawn::pawn(QWidget *parent):chess(parent){

}

pawn::~pawn(){

}

void pawn::decideAvailbleMoves(QList <QPushButton *>l, chess *chessOnBoard[10][9],QPoint coordinate[10][9],int rx,int ry,int bx,int by){
    bool kingToKing = false;
    int chessBetween = 0;
    for (int i = by;i <= ry;++i) {//check king to king
        if(chessOnBoard[i][rx] != nullptr){
            chessBetween++;
        }
    }
    if(rx == bx && rx == getPos().second && chessBetween == 3){
        kingToKing = true;
    }

    if(getPlayer() == 0){//black pawn
        if(getPos().first <= 4){//before middle line
            generateAvailbleMove(getPos().first + 1,getPos().second,l,1,chessOnBoard,getPlayer(),coordinate);//move forward
        }
        else{
            generateAvailbleMove(getPos().first + 1,getPos().second,l,1,chessOnBoard,getPlayer(),coordinate);//move forward
            if(!kingToKing){
                generateAvailbleMove(getPos().first,getPos().second + 1,l,2,chessOnBoard,getPlayer(),coordinate);//move right
                generateAvailbleMove(getPos().first,getPos().second - 1,l,3,chessOnBoard,getPlayer(),coordinate);//move left
            }
        }
    }
    else{//red pawn
        if(getPos().first >= 5){//before middle line
            generateAvailbleMove(getPos().first - 1,getPos().second,l,1,chessOnBoard,getPlayer(),coordinate);//move forward
        }
        else{
            generateAvailbleMove(getPos().first - 1,getPos().second,l,1,chessOnBoard,getPlayer(),coordinate);//move forward
            if(!kingToKing){
                generateAvailbleMove(getPos().first,getPos().second + 1,l,2,chessOnBoard,getPlayer(),coordinate);//move right
                generateAvailbleMove(getPos().first,getPos().second - 1,l,3,chessOnBoard,getPlayer(),coordinate);//move left
            }
        }
    }
}
