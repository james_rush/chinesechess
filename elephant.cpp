#include "elephant.h"

elephant::elephant(QWidget *parent):chess(parent){

}

elephant::~elephant(){

}

void elephant::decideAvailbleMoves(QList <QPushButton *>l, chess *chessOnBoard[10][9],QPoint coordinate[10][9],int rx,int ry,int bx,int by){
    bool kingToKing = false;
    int chessBetween = 0;
    for (int i = by;i <= ry;++i) {//check king to king
        if(chessOnBoard[i][rx] != nullptr){
            chessBetween++;
        }
    }
    if(rx == bx && rx == getPos().second && chessBetween == 3){
        kingToKing = true;
    }
    if(!kingToKing){
        if(getPlayer() == 0){//black elephant
            //+y+x
            if(chessOnBoard[getPos().first + 1][getPos().second + 1] == nullptr && getPos().first + 2 <= 4){//special rule
                generateAvailbleMove(getPos().first + 2,getPos().second + 2,l,1,chessOnBoard,getPlayer(),coordinate);
            }
            //-y+x
            if(chessOnBoard[getPos().first - 1][getPos().second + 1] == nullptr && getPos().first - 2 <= 4){//special rule
                generateAvailbleMove(getPos().first - 2,getPos().second + 2,l,2,chessOnBoard,getPlayer(),coordinate);
            }
            //-y-x
            if(chessOnBoard[getPos().first - 1][getPos().second - 1] == nullptr && getPos().first - 2 <= 4){//special rule
                generateAvailbleMove(getPos().first - 2,getPos().second - 2,l,3,chessOnBoard,getPlayer(),coordinate);
            }
            //+y-x
            if(chessOnBoard[getPos().first + 1][getPos().second - 1] == nullptr && getPos().first + 2 <= 4){//special rule
                generateAvailbleMove(getPos().first + 2,getPos().second - 2,l,4,chessOnBoard,getPlayer(),coordinate);
            }
        }
        else {
            //+y+x
            if(chessOnBoard[getPos().first + 1][getPos().second + 1] == nullptr && getPos().first + 2 >= 5){//special rule
                generateAvailbleMove(getPos().first + 2,getPos().second + 2,l,1,chessOnBoard,getPlayer(),coordinate);
            }
            //-y+x
            if(chessOnBoard[getPos().first - 1][getPos().second + 1] == nullptr && getPos().first - 2 >= 5){//special rule
                generateAvailbleMove(getPos().first - 2,getPos().second + 2,l,2,chessOnBoard,getPlayer(),coordinate);
            }
            //-y-x
            if(chessOnBoard[getPos().first - 1][getPos().second - 1] == nullptr && getPos().first - 2 >= 5){//special rule
                generateAvailbleMove(getPos().first - 2,getPos().second - 2,l,3,chessOnBoard,getPlayer(),coordinate);
            }
            //+y-x
            if(chessOnBoard[getPos().first + 1][getPos().second - 1] == nullptr && getPos().first + 2 >= 5){//special rule
                generateAvailbleMove(getPos().first + 2,getPos().second - 2,l,4,chessOnBoard,getPlayer(),coordinate);
            }
        }
    }
}
