#include "guard.h"

guard::guard(QWidget *parent):chess(parent){

}

guard::~guard(){

}

void guard::decideAvailbleMoves(QList <QPushButton *>l, chess *chessOnBoard[10][9],QPoint coordinate[10][9],int rx,int ry,int bx,int by){
    bool kingToKing = false;
    int chessBetween = 0;
    for (int i = by;i <= ry;++i) {//check king to king
        if(chessOnBoard[i][rx] != nullptr){
            chessBetween++;
        }
    }
    if(rx == bx && rx == getPos().second && chessBetween == 3){
        kingToKing = true;
    }
    if(!kingToKing){
        if(getPlayer() == 0){
            //+y+x
            if(getPos().first + 1 <= 2 && getPos().second + 1 <= 5 && getPos().second + 1 >= 3){
                generateAvailbleMove(getPos().first + 1,getPos().second + 1,l,1,chessOnBoard,getPlayer(),coordinate);
            }
            //-y+x
            if(getPos().first - 1 <= 2 && getPos().second + 1 <= 5 && getPos().second + 1 >= 3){
                generateAvailbleMove(getPos().first - 1,getPos().second + 1,l,2,chessOnBoard,getPlayer(),coordinate);
            }
            //-y-x
            if(getPos().first - 1 <= 2 && getPos().second - 1 <= 5 && getPos().second - 1 >= 3){
                generateAvailbleMove(getPos().first - 1,getPos().second - 1,l,3,chessOnBoard,getPlayer(),coordinate);
            }
            //+y-x
            if(getPos().first + 1 <= 2 && getPos().second - 1 <= 5 && getPos().second - 1 >= 3){
                generateAvailbleMove(getPos().first + 1,getPos().second - 1,l,4,chessOnBoard,getPlayer(),coordinate);
            }
        }
        else {
            //+y+x
            if(getPos().first + 1 >= 7 && getPos().second + 1 <= 5 && getPos().second + 1 >= 3){
                generateAvailbleMove(getPos().first + 1,getPos().second + 1,l,1,chessOnBoard,getPlayer(),coordinate);
            }
            //-y+x
            if(getPos().first - 1 >= 7 && getPos().second + 1 <= 5 && getPos().second + 1 >= 3){
                generateAvailbleMove(getPos().first - 1,getPos().second + 1,l,2,chessOnBoard,getPlayer(),coordinate);
            }
            //-y-x
            if(getPos().first - 1 >= 7 && getPos().second - 1 <= 5 && getPos().second - 1 >= 3){
                generateAvailbleMove(getPos().first - 1,getPos().second - 1,l,3,chessOnBoard,getPlayer(),coordinate);
            }
            //+y-x
            if(getPos().first + 1 >= 7 && getPos().second - 1 <= 5 && getPos().second - 1 >= 3){
                generateAvailbleMove(getPos().first + 1,getPos().second - 1,l,4,chessOnBoard,getPlayer(),coordinate);
            }
        }
    }
}
