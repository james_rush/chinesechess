#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QPushButton>
#include <QDebug>
#include <QPoint>
#include <QFile>
#include "chess.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    void initializeChesses();
    void manageMoveBlocks();
    void manageChess();
    void hideAllMoveBlocks();
    void checkEat(int y,int x);
    void changeTurn();
    bool checkWin();
    QList <QPushButton *>availbleMoves;
    QList <chess *>blackChess;
    QList <chess *>redChess;
    QPoint  coordinate[10][9];//(Y,X)
private slots:


    void pawn_clicked();
    void rook_clicked();
    void horse_clicked();
    void elephant_clicked();
    void guard_clicked();
    void cannon_clicked();
    void king_clicked();
    void availblemove_clicked();

    void on_restart_clicked();

    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

private:
    Ui::MainWindow *ui;
    chess *chessOnBoard[10][9];//(Y,X)
    chess *toMove;
    bool whoseTurn;

};

#endif // MAINWINDOW_H
