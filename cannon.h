#ifndef CANNON_H
#define CANNON_H

#include "chess.h"
#include <QPushButton>

class cannon:public chess
{
    Q_OBJECT
public:
    explicit cannon(QWidget *parent = nullptr);
    ~cannon();
    void decideAvailbleMoves(QList <QPushButton *>l,chess *chessOnBoard[10][9],QPoint coordinate[10][9],int rx,int ry,int bx,int by);
};

#endif // CANNON_H
