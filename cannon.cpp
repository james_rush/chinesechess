#include "cannon.h"
#include <QDebug>

cannon::cannon(QWidget *parent):chess(parent){

}

cannon::~cannon(){

}

void cannon::decideAvailbleMoves(QList <QPushButton *>l, chess *chessOnBoard[10][9],QPoint coordinate[10][9],int rx,int ry,int bx,int by){
    int movesForPositiveY,movesForNegativeY,movesForPositiveX,movesForNegativeX,totalMoves = 0;
    chess *fly = nullptr;
    bool flyable = false;
    bool kingToKing = false;
    int chessBetween = 0;
    int fort = 0;
    for (int i = by;i <= ry;++i) {//check king to king
        if(chessOnBoard[i][rx] != nullptr){
            chessBetween++;
        }
    }
    if(rx == bx && rx == getPos().second && chessBetween == 3){
        kingToKing = true;
    }

    for(movesForPositiveY = 1;movesForPositiveY < 10 ;++movesForPositiveY){//check how many moves availble in +y
        if(chessOnBoard[getPos().first + movesForPositiveY][getPos().second] != nullptr || getPos().first + movesForPositiveY == 9){
            break;
        }
    }

    for(int i = 1;i <= movesForPositiveY;++i){//set red block on +y
        if(chessOnBoard[getPos().first + i][getPos().second] == nullptr){//check cannon fort
            generateAvailbleMove(getPos().first + i,getPos().second,l,i,chessOnBoard,getPlayer(),coordinate);
        }
    }

    totalMoves += movesForPositiveY;//refresh total moves

    for(int i = getPos().first;i < 10;++i){//check fly
        if(chessOnBoard[i][getPos().second] != nullptr){
            fly = chessOnBoard[i][getPos().second];
            fort++;
        }
        if(fort == 3){//itself + fort + target
            flyable = true;
            break;
        }
    }

    if(!kingToKing && flyable){
        generateAvailbleMove(fly->getPos().first,fly->getPos().second,l, totalMoves + 1,chessOnBoard,getPlayer(),coordinate);
        totalMoves++;
        fly = nullptr;//reuse pointer
        flyable = false;
        fort = 0;
    }
    else{
        fly = nullptr;//reuse pointer
        flyable = false;
        fort = 0;
    }

    //+x
    for(movesForPositiveX = 1;movesForPositiveX < 9;++movesForPositiveX){//check how many moves availble in +x
        if(chessOnBoard[getPos().first ][getPos().second + movesForPositiveX] != nullptr || getPos().second + movesForPositiveX == 8){
            break;
        }
    }

    for(int i = 1;i <= movesForPositiveX;++i){//set red block on +x
        if(!kingToKing && chessOnBoard[getPos().first][getPos().second + i] == nullptr){//check hing to king and fort
            generateAvailbleMove(getPos().first,getPos().second + i,l, totalMoves + i,chessOnBoard,getPlayer(),coordinate);
        }
    }

    totalMoves += movesForPositiveX;//refresh total moves

    for(int i = getPos().second;i < 9;++i){//check fly
        if(chessOnBoard[getPos().first][i] != nullptr){
            fly = chessOnBoard[getPos().first][i];
            fort++;
        }
        if(fort == 3){
            flyable = true;
            break;
        }
    }

    if(flyable && !(kingToKing)){
        generateAvailbleMove(fly->getPos().first,fly->getPos().second,l, totalMoves + 1,chessOnBoard,getPlayer(),coordinate);
        totalMoves++;
        fly = nullptr;//reuse pointer
        flyable = false;
        fort = 0;
    }
    else{
        fly = nullptr;//reuse pointer
        flyable = false;
        fort = 0;
    }

    //-y
    for(movesForNegativeY = 1;movesForNegativeY < 10;++movesForNegativeY){//check how many moves availble in -y
        if(chessOnBoard[getPos().first - movesForNegativeY ][getPos().second] != nullptr || getPos().first - movesForNegativeY == 0){
            break;
        }
    }

    for(int i = 1;i <= movesForNegativeY;++i){//set red block on -y
        if(chessOnBoard[getPos().first - i][getPos().second] == nullptr){//check fort
            generateAvailbleMove(getPos().first - i,getPos().second,l, totalMoves + i,chessOnBoard,getPlayer(),coordinate);
        }
    }

    totalMoves += movesForNegativeY;//refresh total moves

    for(int i = getPos().first;i >= 0;--i){//check fly
        if(chessOnBoard[i][getPos().second] != nullptr){
            fly = chessOnBoard[i][getPos().second];
            fort++;
            //qDebug()<< fort  << endl;
        }
        if(fort == 3){
            flyable = true;
            break;
        }
    }

    if(!kingToKing && flyable){
        generateAvailbleMove(fly->getPos().first,fly->getPos().second,l, totalMoves + 1,chessOnBoard,getPlayer(),coordinate);
        totalMoves++;
        fly = nullptr;//reuse pointer
        flyable = false;
        fort = 0;
    }
    else{
        fly = nullptr;//reuse pointer
        flyable = false;
        fort = 0;
    }

    //-x
    for(movesForNegativeX = 1;movesForNegativeX < 9 ;++movesForNegativeX){//check how many moves availble in -x
        if(chessOnBoard[getPos().first][getPos().second - movesForNegativeX] != nullptr || getPos().second - movesForNegativeX == 0){//check obsticle on road and edges
            break;
        }
    }
    for(int i = 1;i <= movesForNegativeX;++i){//set red block on -x
        if(!kingToKing && chessOnBoard[getPos().first][getPos().second - i] == nullptr){
            generateAvailbleMove(getPos().first,getPos().second - i,l, totalMoves + i,chessOnBoard,getPlayer(),coordinate);
        }
    }

    for(int i = getPos().second;i >= 0;--i){//check fly
        if(chessOnBoard[getPos().first][i] != nullptr){
            fly = chessOnBoard[getPos().first][i];
            fort++;
        }
        if(fort == 3){
            flyable = true;
            break;
        }
    }

    if(flyable && !(kingToKing)){
        generateAvailbleMove(fly->getPos().first,fly->getPos().second,l, totalMoves + 1,chessOnBoard,getPlayer(),coordinate);
        totalMoves++;
        fly = nullptr;//reuse pointer
        flyable = false;
        fort = 0;
    }
    else{
        fly = nullptr;//reuse pointer
        flyable = false;
        fort = 0;
    }
}

